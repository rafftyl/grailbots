﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponSpray : WeaponRanged
{
	public float sprayAngle;

	public override HitGeometry GetHitGeometry (Vector3 targetPos)
	{
		//sphere fragment
		return new GeometrySubsphere (transform.position, Data.Range, sprayAngle, targetPos - transform.position);
	}
}
