﻿using UnityEngine;

/// <summary>
/// Base class for all weapons in game.
/// </summary>
public abstract class Weapon : MonoBehaviour
{
	[SerializeField] private WeaponData data;
	private int currentAmmo;

	/// <summary>
	/// Weapon handle to be aligned with character's weapon handle
	/// </summary>
	[Tooltip("Weapon handle to be aligned with character's weapon handle")]
	[SerializeField] private Transform handle;
	
	public int MaxAmmo => data.MaxAmmo;
	/// <summary>
	/// Gets or sets a value indicating whether this weapon is picked by a character.
	/// </summary>
	/// <value><c>true</c> if this weapon is picked; otherwise, <c>false</c>.</value>
	public bool IsPicked{ get; set; }
	public WeaponData Data => data;
	public Transform Handle => handle;

	public int CurrentAmmo
    { 
		get => currentAmmo;
		set => currentAmmo = Mathf.Min (value, MaxAmmo);
    }

	/// <summary>
	/// Called after being  equipped by  a character.
	/// </summary>
	public void OnEquip()
    {
		if (!IsPicked)
        {
			IsPicked = true;
		}
	}

	/// <summary>
	/// A function defining hit geometry for a weapon
	/// </summary>
	/// <returns>The hit geometry.</returns>
	/// <param name="targetPos">Target position (mouse position in  case of player, player position in case of enemies).</param>
	public abstract HitGeometry GetHitGeometry (Vector3 targetPos);

	public virtual void Shoot(LayerMask targetLayers, Vector3 targetPos)
    {
		//get hits
		var hits = GetHitGeometry (targetPos).GetHits<IHittable> (targetLayers);
		if (hits == null) {
			return;
		}
		//apply damage
		foreach (var hit in hits) {
			hit.Apply (data.Damage);
		}
	}
	
	private void OnTriggerEnter(Collider col)
	{
		//if the weapon is lying on the ground, and the player walks over it, pick it up
		if (!IsPicked)
		{
			Character player = col.GetComponent<Character> ();
			if (player != null)
			{
				player.PickUpWeapon (this);
			}
		}
	}
		
	private void Awake()
	{
		CurrentAmmo = MaxAmmo;
	}
	
	private void Update()
    {
		if (!IsPicked)
        {
			transform.Rotate (Vector3.up * 60 * Time.deltaTime);
		}
	}
}
