﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponMelee : Weapon
{
	public override HitGeometry GetHitGeometry(Vector3 targetPos)
    {
		return new GeometryRay (transform.position, (targetPos - transform.position).Flat(), Data.Range); 
	}
}
