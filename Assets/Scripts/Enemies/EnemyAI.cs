﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The base class for all the enemies
/// </summary>
[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))] 
[RequireComponent(typeof(Character))] 
public class EnemyAI : MonoBehaviour
{
	/// <summary>
	/// Maximum player speed that can be perceived by this character - if the player moves faster, this character will lag behind while aiming 
	/// </summary>
	[Tooltip("Maximum player speed that can be perceived by this character - if the player moves faster, this character will lag behind while aiming")]
	[SerializeField] private float positionPerceptionSpeed = 5f;
	/// <summary>
	/// Nav Mesh Agent for navigation 
	/// </summary>
	private UnityEngine.AI.NavMeshAgent navmeshAgent;

	/// <summary>
	/// This character will attack when the distance from player is less than (1 - mRangeMargin) * weaponRange 
	/// </summary>
	private float rangeMargin = 0.1f;
	private Vector3 peceivedTargetPosition;
	private Character character;
	private Character target;

	public Character Target
	{
		get => target;
		set
		{
			target = value;
			peceivedTargetPosition = value?.transform.position ?? Vector3.zero;
		}
	}

	private void Start()
	{
		character = GetComponent<Character>();
		if (character.CurrentWeapon != null)
		{
			//TODO: not the best place for such things
			character.CurrentWeapon.CurrentAmmo = 10000;
		}
		navmeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		navmeshAgent.updateRotation = false;	
		navmeshAgent.Warp (transform.position);
	}
	
	protected virtual void Update()
	{
		if (Target == null)
		{
			return;
		}
		
		peceivedTargetPosition = Vector3.MoveTowards (peceivedTargetPosition, Target.transform.position, positionPerceptionSpeed * Time.deltaTime);
		if (character.CurrentWeapon != null && Vector3.Distance (Target.transform.position, transform.position) > character.CurrentWeapon.Data.Range * (1 - rangeMargin))
        { 
			navmeshAgent.SetDestination (Target.transform.position);
		}
        else if(!character.IsShootingLocked)
        {
			navmeshAgent.SetDestination (transform.position);
			character.Shoot(peceivedTargetPosition);
		}

		transform.rotation = Quaternion.LookRotation ((peceivedTargetPosition - transform.position).Flat ());
	}
}
