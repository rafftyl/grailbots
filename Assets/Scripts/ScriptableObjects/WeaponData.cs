﻿using UnityEngine;

/// <summary>
/// Scriptable object containing basic weapon data (needed for inventory and UI purposes)
/// </summary>
[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
	[SerializeField] private string weaponName = "";
	[SerializeField] private string weaponDescription = "";
	[SerializeField] private Sprite icon = null;
	[SerializeField] private int damage = 10;
	[SerializeField] private float recoilTime = 1;
	[SerializeField] private float range = 20;
	[SerializeField] private int maxAmmo = 10;

	public string WeaponName => weaponName;
	public string WeaponDescription => weaponDescription;
	public Sprite Icon => icon;
	public int Damage => damage;
	public float RecoilTime => recoilTime;
	public float Range => range;
	public int MaxAmmo => maxAmmo;
}
