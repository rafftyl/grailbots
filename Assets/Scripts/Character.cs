﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Base class for every character in game - player and his opponents
/// </summary>
public class Character : MonoBehaviour, IHittable
{
	/// <summary>
	/// Maximum health points of the character
	/// </summary>
	[Tooltip("Maximum health points of the character")]
	[SerializeField] private int maxHealth = 100;

	/// <summary>
	/// Layers containing objects that can be hit by this character
	/// </summary>
	[Tooltip("Layers containing objects that can be hit by this character")]
	[SerializeField] private LayerMask hittableLayers = 1;

	/// <summary>
	/// Renderers to be highlighted when the character is hit or aimed at - materials of these renderers have to use RimHighlighted shader
	/// </summary>
	[Tooltip("Renderers to be highlighted when the character is hit or aimed at - materials of these renderers have to use RimHighlighted shader")]
	[SerializeField] private List<Renderer> highlightedRenderers = null;

	/// <summary>
	/// Blood particle system prefab
	/// </summary>
	[Tooltip("Blood particle system prefab")]
	[SerializeField] private ParticleSystem bloodParticlePrefab = null;

	/// <summary>
	/// The transform that this character's weapon will be parented to
	/// </summary>
	[Tooltip("The transform that this character's weapon will be parented to")]
	[SerializeField] private Transform weaponHandle = null;

	/// <summary>
	/// Character's weapon inventory.
	/// </summary>
	[Tooltip("Character's weapon inventory.")]
	[SerializeField] private List<Weapon> weapons = null;
	
	/// <summary>
	/// Instanced blood particles (taken from bloodParticlePrefab)
	/// </summary>
	private ParticleSystem bloodParticles;
	private Animator animator;

	/// <summary>
	/// The index of the m current weapon.
	/// </summary>
	private int currentWeaponIndex;

	/// <summary>
	/// Helper flag for highlighting renderers
	/// </summary>
	private bool wasHighlighted = false;
	private float highlightTime;
	private float highlightTimer;

	public IReadOnlyList<Weapon> Weapons => weapons;
	/// <summary>
	/// Gets the current weapon.
	/// </summary>
	/// <value>The current weapon.</value>
	public Weapon CurrentWeapon => (currentWeaponIndex < weapons.Count && currentWeaponIndex >-1) ? weapons [currentWeaponIndex] : null;

	/// <summary>
	/// Gets or sets current health.
	/// </summary>
	/// <value>Current health.</value>
	public int CurrentHealth { get; protected set; }
	
	/// <summary>
	/// A flag used to determine if the character is ready to shoot 
	/// </summary>
	public bool IsShootingLocked { get; private set; }
	
	/// <summary>
	/// Picks up a weapon.
	/// </summary>
	/// <param name="weapon">Weapon.</param>
	public void PickUpWeapon(Weapon weapon)
	{
		//check if you already have this type of weapon
		int index = weapons.FindIndex(inventoryWeapon => weapon.Data == inventoryWeapon.Data);

		if (index > -1) //if you do, grab ammo and destroy the weapon
		{
			Weapon wp = weapons [index];
			if (wp.CurrentAmmo < wp.MaxAmmo)
			{
				wp.CurrentAmmo += weapon.CurrentAmmo;	
				Destroy (weapon.gameObject);
			}
		}
		else //if not, pick it up
		{ 
			weapons.Add (weapon);
			weapon.IsPicked = true;
			EquipWeapon (weapons.Count - 1);
		}
	}
	
	/// <summary>
	/// Shoots in the direction of targetPos
	/// </summary>
	/// <param name="targetPos">Target position.</param>
	public virtual void Shoot(Vector3 targetPos)
	{
		if (!IsShootingLocked)
		{
			CurrentWeapon.Shoot (hittableLayers, targetPos);
			PlayAttackAnimation ();
			StartCoroutine (ShootLockCoroutine ());
		}
	}
	
	//IHittable implementation:
	public virtual void OnHit(int damage, Vector3 hitPoint, Vector3 hitDirection)
	{
		CurrentHealth -= damage;
		//Debug.Log (name + " has been hit for " + damage.ToString () + " damage.");
		if (CurrentHealth <= 0)
		{
			Die();
		}
		Highlight (1, 0.1f);

		//if there is a blood particle  system assigned, instantiate it's clone and play
		if (bloodParticlePrefab != null)
		{
			if (bloodParticles == null)
			{
				bloodParticles = Instantiate<ParticleSystem> (bloodParticlePrefab);
				bloodParticles.transform.SetParent (transform);
			}

			bloodParticles.transform.position = hitPoint;
			bloodParticles.transform.rotation = Quaternion.LookRotation (hitDirection);
			bloodParticles.Play ();
		}
	}
	
	/// <summary>
	/// Highlight the renderers with highlightStrength for highlightTime.
	/// </summary>
	/// <param name="highlightStrength">Highlight strength.</param>
	/// <param name="highlightTime">Highlight time.</param>
	public void Highlight(float highlightStrength = 1, float highlightTime = 0)
	{
		//if the renderer's are not highlighted or we want to increase the highlight time
		if (!wasHighlighted || highlightTime > this.highlightTime)
		{
			foreach (Renderer r in highlightedRenderers)
			{
				r.material.SetFloat ("_HighlightStrength", highlightStrength); //_HighlightStrength is defined  in RimHighlight shader
			}
			highlightTimer = 0;
			this.highlightTime = highlightTime;
			wasHighlighted = true;
		}
	}

	/// <summary>
	/// Equips the weapon under given index in the weapon list.
	/// </summary>
	/// <param name="weaponIndex">Weapon index.</param>
	protected virtual void EquipWeapon(int weaponIndex)
    {
		if (weaponIndex > -1 && weaponIndex < weapons.Count)
        {
			//disable current weapon
			if (CurrentWeapon != null)
            {
				CurrentWeapon.gameObject.SetActive (false);
			}
			//set, position and parent the specified weapon
			currentWeaponIndex = weaponIndex;
			CurrentWeapon.transform.rotation = weaponHandle.rotation;
			CurrentWeapon.transform.position = weaponHandle.position + (CurrentWeapon.transform.position - CurrentWeapon.Handle.position);
			CurrentWeapon.transform.SetParent (weaponHandle);
			CurrentWeapon.gameObject.SetActive (true);

			//let the weapon know that it's been equipped
			CurrentWeapon.OnEquip ();
		}
	}

	/// <summary>
	/// Unhighlight the renderers
	/// </summary>
	private void Unhighlight()
    {
		foreach (Renderer r in highlightedRenderers)
        {
			r.material.SetFloat ("_HighlightStrength", 0);
		}
	}
	
	/// <summary>
	/// This function is called on character's death
	/// </summary>
	private void Die ()
	{
		Destroy (gameObject);
	}

	private void Start()
    {
	    CurrentHealth = maxHealth;
	    EquipWeapon (0); //equip the first weapon in the character's inventory
	    animator = GetComponent<Animator> ();
	}

	private IEnumerator ShootLockCoroutine()
    {
		IsShootingLocked = true;
		yield return new WaitForSeconds (CurrentWeapon.Data.RecoilTime);
		IsShootingLocked = false;
	}

	private void PlayAttackAnimation()
    {
		if (animator != null)
        {
			animator.SetTrigger ("Attack");
		}
	}

	private void LateUpdate()
    {
		if (!wasHighlighted)
        {
			Unhighlight ();
			return;
		}

		highlightTimer += Time.deltaTime;
		if (highlightTimer > highlightTime)
        {
			wasHighlighted = false;
		}
	}
}
