﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// User interface element containing weapon slots
/// </summary>
public class UIWeaponPanel : MonoBehaviour
{
	//TODO: react to agent selection
	[SerializeField] private List<UIWeaponSlot> weaponSlots;
	void OnEnable()
    {
		//GameEvents.OnPlayerInventoryChanged += RefreshSlots;
	}

	void OnDisable()
    {
		//GameEvents.OnPlayerInventoryChanged -= RefreshSlots;
	}

	void RefreshSlots(Character character)
    {
		for (int i = 0; i < weaponSlots.Count; ++i)
        {
			if (i < character.Weapons.Count)
            {
				weaponSlots [i].SetWeapon (character.Weapons [i]);
				if (character.CurrentWeapon == character.Weapons [i])
                {
					weaponSlots [i].Highlight(); //highlight active weapon
				}
                else
                {
					weaponSlots [i].Unhighlight();//unhighlight inactive weapon
				}
			}
            else
            {
				weaponSlots [i].Clear();//if there's no weapon, clear the slot
			}
		}
	}
}
