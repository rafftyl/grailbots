﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// User interface weapon slot.
/// </summary>
public class UIWeaponSlot : MonoBehaviour
{
	/// <summary>
	/// Image overriden with weapon icon
	/// </summary>
	[Tooltip("Image overriden with weapon icon")]
	[SerializeField] private Image iconImage = null;
	[SerializeField] private Image backgroundImage = null;

	/// <summary>
	/// The color of the active slot.
	/// </summary>
	[Tooltip("The color of the active slot.")]
	[SerializeField] private Color highlightColor = Color.green;

	/// <summary>
	/// The color of the inactive slot.
	/// </summary>
	[Tooltip("The color of the inactive slot.")]
	[SerializeField] private Color normalColor = Color.white;

	/// <summary>
	/// Text field containing the name of the weapon
	/// </summary>
	[Tooltip("Text field containing the name of the weapon")]
	[SerializeField] private Text weaponName = null;

	/// <summary>
	/// The ammo slider
	/// </summary>
	[Tooltip("The ammo slider")]
	[SerializeField] private Slider slider = null;
	
	private Weapon weapon;
	
	public void SetWeapon(Weapon weapon)
    {
		//activate ui elements and fill them with data from the weapon
		iconImage.gameObject.SetActive (true);
		slider.gameObject.SetActive (true);
		weaponName.gameObject.SetActive (true);
		weaponName.text = weapon.Data.WeaponName;
		this.weapon = weapon;
		slider.maxValue = weapon.Data.MaxAmmo;
		slider.wholeNumbers = true;
		iconImage.sprite = weapon.Data.Icon;
	}

	public void Highlight()
    {
		iconImage.color = backgroundImage.color = highlightColor;
	}

	public void Unhighlight()
    {
		iconImage.color = backgroundImage.color = normalColor;
	}

	public void Clear()
    {
		//deactivate unnecessary ui elements
		iconImage.gameObject.SetActive (false);
		slider.gameObject.SetActive (false);
		weaponName.gameObject.SetActive (false);
		Unhighlight ();
	}
	
	private void Update()
	{
		if (weapon != null)
		{
			slider.value = weapon.CurrentAmmo;
		}
	}
}
