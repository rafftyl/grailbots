﻿using UnityEngine;

public static class AimEffects
{
	/// <summary>
	/// Highlights enemies in weapon's range.
	/// </summary>
	/// <param name="weapon"></param>
	/// <param name="targetLayers">Target layers.</param>
	/// <param name="targetPos"></param>
	public static void HighlightEnemies(Weapon weapon, LayerMask targetLayers, Vector3 targetPos)
	{
		var targets = weapon.GetHitGeometry(targetPos).GetHits<Character> (targetLayers);
		if (targets == null)
		{
			return;
		}
		foreach (var target in targets)
		{
			target.Hittable.Highlight(0.4f);
		}
	}
}
