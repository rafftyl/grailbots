﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spherical hit geometry
/// </summary>
public class GeometrySphere : HitGeometry
{
	protected Vector3 center;
	protected float radius;
	public GeometrySphere(Vector3 center, float radius)
	{
		this.center = center;
		this.radius = radius;
	}
	protected override List<Hit<IHittable>> GetRaycastHits (LayerMask targetMask)
	{
		Collider[] collidersInSphere = Physics.OverlapSphere (center, radius, targetMask, QueryTriggerInteraction.Ignore);
		List<Hit<IHittable>> result = new List<Hit<IHittable>>();
		foreach (Collider col in collidersInSphere)
		{
			IHittable hittable = col.GetComponent<IHittable> ();
			if (hittable == null)
			{
				continue;
			}
			result.Add (new Hit<IHittable> (hittable, col.transform.position, (col.transform.position - center).normalized));		
		}

		return result;
	}
}