﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sphere fragment hit geometry
/// </summary>
public class GeometrySubsphere : GeometrySphere
{	
	float maxAngle;
	Vector3 forward;
	public GeometrySubsphere(Vector3 center, float radius, float maxAngle, Vector3 direction) : base(center,radius)
	{		
		this.maxAngle = maxAngle;
		forward = direction;
	}
	protected override List<Hit<IHittable>>  GetRaycastHits (LayerMask targetMask)
	{
		//Check hittables within a sphere, but also check if the angle is less then specified maximum value
		Collider[] collidersInSphere = Physics.OverlapSphere (center, radius, targetMask, QueryTriggerInteraction.Ignore);
		List<Hit<IHittable>>  result = new List<Hit<IHittable>> ();
		foreach (Collider col in collidersInSphere)
		{
			IHittable hittable = col.GetComponent<IHittable> ();
			if (hittable == null)
			{
				continue;
			}

			Vector3 posDif = col.transform.position - center;
			if (Vector3.Angle (forward, posDif) < maxAngle)
			{
				RaycastHit hit;
				if (Physics.Raycast (center, posDif,out hit, posDif.magnitude, targetMask, QueryTriggerInteraction.Ignore))
				{
					if (hit.collider == col)
					{
						result.Add (new Hit<IHittable> (hittable, hit.point, (hit.point - center).normalized));
					}
				}
			}
		}

		return result;
	}
}