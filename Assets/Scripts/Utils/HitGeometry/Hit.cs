﻿using UnityEngine;

/// <summary>
/// A struct containing hit data.
/// </summary>
public class Hit<T> where T : class, IHittable
{
	public T Hittable { get; }
	public Vector3 HitPoint { get; }
	public Vector3 HitDirection { get; }
	
	public Hit(T hittable, Vector3 hitPoint, Vector3  hitDir)
	{
		Hittable = hittable;
		HitPoint = hitPoint;
		HitDirection = hitDir;
	}
	
	public void Apply(int damage)
	{
		Hittable.OnHit (damage, HitPoint, HitDirection);
	}
}