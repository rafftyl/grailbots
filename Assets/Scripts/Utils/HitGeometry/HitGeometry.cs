﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Base class for hit checking
/// </summary>
public abstract class HitGeometry
{	
	protected abstract List<Hit<IHittable>> GetRaycastHits (LayerMask targetMask);

	/// <summary>
	/// Gets hittables of a specified derived type
	/// </summary>
	/// <returns>The hits.</returns>
	/// <param name="targetMask">Target mask.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public List<Hit<T>> GetHits<T>(LayerMask targetMask) where T: class, IHittable
    {
		var hits = GetRaycastHits (targetMask);
		if (hits == null)
        {
			return null;
		}

		List<Hit<T>> result =  new List<Hit<T>>();
		foreach (var hit in hits)
        {			
			if (hit.Hittable is T castHittable)
            {
				result.Add (new Hit<T>(castHittable, hit.HitPoint, hit.HitDirection));
			}
		}

		return result;
	}
}