﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple ray
/// </summary>
public class GeometryRay : HitGeometry
{
	readonly Vector3 origin;
	readonly Vector3 direction;
	readonly float range;
	
	public GeometryRay(Vector3 origin, Vector3 direction, float range)
	{
		this.range = range;
		this.origin = origin;
		this.direction = direction;
	}
		
	protected override List<Hit<IHittable>>  GetRaycastHits (LayerMask targetMask)
	{
		if (Physics.Raycast (origin, direction, out var hit, range, targetMask, QueryTriggerInteraction.Ignore))
		{
			IHittable hittable = hit.collider.GetComponent<IHittable> ();
			if (hittable == null)
			{
				return null;
			}

			return new List<Hit<IHittable>>  { new Hit<IHittable> (hittable, hit.point, direction) };
		}
		return null;
	}
}