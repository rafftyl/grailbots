﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A singleton governing enemy waves
/// </summary>
public class EnemyManager : MonoBehaviour
{
	/// <summary>
	/// A helper struct used for choosing enemies to spawn 
	/// </summary>
	[System.Serializable]
	public class EnemyProbabilityPair
    {
		[SerializeField] private EnemyAI enemy;
		[SerializeField] private float probability;

		public EnemyAI Enemy => enemy;

		public float Probability
		{
			get => probability;
			set => probability = value;
		}
    }

	/// <summary>
	/// Enemy wave.
	/// </summary>
	[System.Serializable]
	public class EnemyWave
	{
		/// <summary>
		/// Array of enemies with their corresponding spawn probabilities 
		/// </summary>
		[SerializeField] private List<EnemyProbabilityPair> enemyData;

		/// <summary>
		/// The enemy count in this wave.
		/// </summary>
		[SerializeField] private int enemyCount;

		/// <summary>
		/// Time needed to spawn all the enemies in this wave
		/// </summary>
		[SerializeField] private float spawnDuration;

		public IReadOnlyList<EnemyProbabilityPair> EnemyData => enemyData;
		public int EnemyCount => enemyCount;
		public float SpawnDuration => spawnDuration;
	}
	
	public int EnemyCount => spawnedEnemies.Count;
	public int WaveNumber => currentWaveIndex + 1;
	public List<EnemyAI> SpawnedEnemies => spawnedEnemies;

	[SerializeField] private List<Transform> spawnPoints = null;
	[SerializeField] private List<EnemyWave> enemyWaveData = null;

	/// <summary>
	/// Collisions with these layers are checked before spawning an enemy 
	/// </summary>
	[Tooltip("Collisions with these layers are checked before spawning an enemy")]
	[SerializeField] private LayerMask collisionCheckLayers;

	/// <summary>
	/// Radius of collision check
	/// </summary>
	[Tooltip("Radius of collision check")]
	[SerializeField] private float collisionCheckRadius;
	[SerializeField] private List<Character> attackedCharacters = null;

	private int currentWaveIndex = -1;
	private float spawnTimestep;
	private int enemiesSpawned = 0;
	private int enemiesKilled = 0;
	private readonly List<EnemyAI> spawnedEnemies = new List<EnemyAI>();

	public EnemyWave CurrentWave => currentWaveIndex < enemyWaveData.Count ? enemyWaveData [currentWaveIndex] : null;

	private void Awake()
    {
		if (spawnPoints.Count > 0 && enemyWaveData.Count > 0)
        {
			StartNextWave ();
		}
	}

	private void StartNextWave()
    {
		currentWaveIndex++;
		enemiesKilled = 0; 
		enemiesSpawned = 0;
		spawnedEnemies.Clear ();
		spawnTimestep = CurrentWave.SpawnDuration /((float) CurrentWave.EnemyCount);
		StartCoroutine (SpawnCoroutine());
	}

	void OnEnable()
    {
	    //TODO
		//GameEvents.OnEnemyKilled += OnEnemyKilled;	
	}

	void OnDisable()
    {
	    //TODO
		//GameEvents.OnEnemyKilled -= OnEnemyKilled;
	}

	/// <summary>
	/// Checks the state of the game after an  enemy is killed and starts another wave/ends the game when necessary
	/// </summary>
	/// <param name="enemy">Enemy killed.</param>
	void OnEnemyKilled(EnemyAI enemy)
    {
		enemiesKilled++;
		//Debug.Log(mEnemiesKilled.ToString() +  " killed of " + CurrentWave.enemyCount);
		spawnedEnemies.Remove (enemy);
		if (enemiesKilled == CurrentWave.EnemyCount)
        {
			if (currentWaveIndex < enemyWaveData.Count - 1)
            {
				StartNextWave ();
			}
        }
	}

	/// <summary>
	/// Removes all the enemies from the level 
	/// </summary>
	void ClearEnemies()
    {
		foreach (EnemyAI enemy in spawnedEnemies)
        {
			Destroy (enemy.gameObject);
		}
		spawnedEnemies.Clear ();
	}

	IEnumerator SpawnCoroutine()
    {
		//while there are enemies left to spawn
		while (CurrentWave != null && enemiesSpawned < CurrentWave.EnemyCount)
        {			
			yield return new WaitForSeconds (spawnTimestep); //wait for a while

			//randomly pick a spawn point
			Transform spawnPoint = spawnPoints [Random.Range (0, spawnPoints.Count)];

			//pick a number from 0-1 range and iterate through the enemy list until their accumulated probability is less than this number
			float rand = Random.Range (0, 1f);
			float accumProbability = 0;
			foreach (EnemyProbabilityPair enemyData in CurrentWave.EnemyData)
            {
				if (enemyData.Probability + accumProbability > rand)
                {
					//if there's anything around the spawn point, don't spawn
					if (!Physics.CheckSphere (spawnPoint.position, collisionCheckRadius, collisionCheckLayers))
                    {
						EnemyAI enemy = Instantiate<EnemyAI> (enemyData.Enemy);
						enemy.Target = attackedCharacters.Find(character => character != null);
						enemy.transform.position = spawnPoint.position;
						enemy.transform.rotation = spawnPoint.rotation;
						spawnedEnemies.Add (enemy);
						enemiesSpawned++;
						//TODO:
						//GameEvents.EnemySpawned (enemy);
					}
					break;					
				}
				accumProbability += enemyData.Probability;
			}
		}
	}

	#if UNITY_EDITOR
	void OnValidate()
    {
		//renormalize  probabilities for each wave
		foreach (EnemyWave waveData in enemyWaveData)
        {
			float sum = 0;
			foreach (EnemyProbabilityPair pair in waveData.EnemyData)
            {
				if (pair.Probability < 0)
                {
					pair.Probability = 0;
				}
				sum += pair.Probability;
			}

			if (sum > 0)
            {
				foreach (EnemyProbabilityPair pair in waveData.EnemyData)
                {
					pair.Probability /= sum;
				}
			}
		}
	}
	#endif
}

