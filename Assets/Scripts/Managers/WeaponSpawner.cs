﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A singleton spawning weapons in defined spawn points
/// </summary>
public class WeaponSpawner : MonoBehaviour
{
	/// <summary>
	/// Weapons that can be spawned. 
	/// </summary>
	[Tooltip("Weapons that can be spawned.")]
	[SerializeField] private List<Weapon> weapons = null;

	/// <summary>
	/// Weapon spawn points.
	/// </summary>
	[Tooltip("Weapon spawn points.")]
	[SerializeField] private List<Transform> weaponSpawnPoints = null;

	/// <summary>
	/// Time between weapon spawn attempts
	/// </summary>
	[Tooltip("Time between weapon spawn attempts.")]
	[SerializeField] private float weaponSpawnPeriod;

	/// <summary>
	/// Layers checked before spawning a weapon
	/// </summary>
	[Tooltip("Layers checked before spawning a weapon.")]
	[SerializeField] private LayerMask collisionCheckLayers;
	[SerializeField] private float collisionCheckRadius;

	private void Awake ()
	{
		if (weaponSpawnPoints.Count > 0 && weapons.Count > 0)
        {
			StartCoroutine(SpawnCoroutine ());
		}
	}

	IEnumerator SpawnCoroutine()
    {
		while (true)
        {			
			yield return new WaitForSeconds (weaponSpawnPeriod);
			Transform spawnPoint = weaponSpawnPoints [Random.Range (0, weaponSpawnPoints.Count)];
			if (!Physics.CheckSphere (spawnPoint.position, collisionCheckRadius, collisionCheckLayers,QueryTriggerInteraction.Collide))
            {
				Weapon wp = weapons [Random.Range (0, weapons.Count)];
				Instantiate (wp.gameObject, spawnPoint.position, spawnPoint.rotation);
			}
		}
	}

}
